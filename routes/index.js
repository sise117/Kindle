var express = require('express');
var router = express.Router();
var fs = require('fs');


var data = require('../database/data.json');

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index',{books:data.books});
})

router.get('/book', function(req, res, next) {
  res.render('book');
});

router.get('/denglu', function(req, res, next) {
  res.render('denglu');
});

router.get('/pass', function(req, res, next) {
  res.render('pass');
});

router.post('/pass2', function(req, res, next) {
  res.render('pass2');
});

router.get('/a',(req,res)=>{
	//data.sites.push();
	//console.log(JSON.stringify(data));
	// fs.writeFile('database/data.json',JSON.stringify(data),(err)=>{
	// 	if(err) console.error(err);
	// });	
	res.json(data);
})

module.exports = router;
